/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package site3;

/**
 *
 * @author Eliška
 */
public class CRC {

    private int crc = 0;
    private int[] crc_table = make_crc_table();

    /**
     * Make the table for a fast CRC.
     */
    
    private int[] make_crc_table() {
        int[] crc_table = new int[256];
        for (int n = 0; n < 256; n++) {
            int c = n;
            for (int k = 8; --k >= 0;) {
                if ((c & 1) != 0) {
                    c = 0xedb88320 ^ (c >>> 1);
                } else {
                    c = c >>> 1;
                }
            }
            crc_table[n] = c;
        }
        return crc_table;
    }
    
    public void reset () { crc = 0; }


    public long update(byte[] buf, int off, int len) {
        int c = ~crc;
        while (--len >= 0) {
            c = crc_table[(c ^ buf[off++]) & 0xff] ^ (c >>> 8);
        }
        crc = ~c;
        
        return (long) crc & 0xffffffffL;
    }

}

