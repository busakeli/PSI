/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package site3;

//PRIJIMA
import java.net.*;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.*;
import java.util.zip.*;

/**
 *
 * @author Eliška
 */
public class Site3 {

    String ClientIP = "127.0.0.1";
    String ServerIP = "127.0.0.1";
    int ClientPort_LISTEN = 1111; //kde posloucha client
    int ClientPort_SEND = 4444; //odkud posila client potvrzeni
    int ServerPort_SEND = 8888; //kde posloucha server
    String receiveFile = "C:\\Users\\Eliška\\Desktop\\CVUT\\APO\\projekt\\prijmout.txt";
    int length = 1024; //delka packetu
    byte[] receiveData = new byte[length + 9];
    byte[] data = new byte[length];
    boolean posledni = false;

    byte[] hashPrijaty = new byte[16];
    byte[] hashSpocitany = new byte[16];

    CRC crcClass = new CRC();
    ByteUt BU = new ByteUt();

    byte[] stopAndWait = new byte[1];
    byte[] chyba = {-3};
    long crc = 0;
    byte[] crcByte = new byte[8];

    boolean stop = false;
    int counter = 0;

    private byte[] longToByte(long x) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeLong(x);
        dos.close();
        return baos.toByteArray();
    }

    private void client() throws IOException, SocketException, InterruptedException, NoSuchAlgorithmException {
        FileOutputStream FOS = new FileOutputStream(receiveFile);

        DatagramSocket clientSocket = new DatagramSocket(ClientPort_LISTEN);

        byte[] receivedCRC = new byte[8];
        while (!stop) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, length + 9);
            clientSocket.receive(receivePacket);
            int L = receiveData.length;

            stopAndWait[0] = receiveData[0]; //rozdeleni pole na 3 casti

            if (stopAndWait[0] == -1) { //posledni packet
                data = new byte[L - 9];
                posledni = true;
            }

            for (int i = 1; i < 9; i++) { //ASI SE NEAKTUALIZUJE DORUCENE CRC!!!
                receivedCRC[(i - 1)] = receiveData[i];
            }

            for (int i = 9; i < L; i++) {
                data[(i - 9)] = receiveData[i];
            }

            crc = crcClass.update(data, 0, length);
            crcClass.reset();
            crcByte = longToByte(crc);

            System.out.println("dorucene CRC: " + receivedCRC);
            System.out.println("Vypocitane CRC: " + crcByte);

            int control = 0; //Kontrola CRC
            for (int i = 0; i < 8; i++) {
                if (receivedCRC[i] != crcByte[i]) {
                    control++;
                }
            }

            if (control < 0) {
                sendBack(chyba);
            } else {
                FOS.write(data);
                if (!posledni) {
                    System.out.println("dorucene Saw: " + stopAndWait[0]);
                    stopAndWait[0] = (byte) (counter);
                } else if (posledni) {
                    stopAndWait[0] = -2; //odeslani potvrzeni konce prenosu
                }
                System.out.println("posila SaW:" + stopAndWait[0]);
                sendBack(stopAndWait);
                counter++;
                if (posledni) {
                    System.out.println("Konec prenosu, juch!");
                    stop = true;
                }
            }

        }
        DatagramPacket receivePacketH;
        receivePacketH = new DatagramPacket(hashPrijaty, 16);
        clientSocket.receive(receivePacketH);
        System.out.println("Prijaty hash: " + hashPrijaty);

        clientSocket.close();
        FOS.close();

        hashSpocitany = getFileChecksum();
        System.out.println("Spocitany hash: " + hashSpocitany);

    }

    private byte[] getFileChecksum() throws IOException, NoSuchAlgorithmException {

        File file = new File(this.receiveFile);
        MessageDigest digest = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        fis.close();
        return digest.digest();
    }

    private void sendBack(byte[] check) throws UnknownHostException, SocketException, IOException, InterruptedException {
        InetAddress IpAddress = InetAddress.getByName(ServerIP);
        DatagramPacket packetToSend = new DatagramPacket(check, 1, IpAddress, ServerPort_SEND);
        DatagramSocket socketToSend = new DatagramSocket(ClientPort_SEND);

        socketToSend.send(packetToSend);
        socketToSend.close();
    }

    public static void main(String[] args) {

        try {
            Site3 zkouska = new Site3();
            zkouska.client();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
