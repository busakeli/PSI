/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package site3;
import java.nio.*;

/**
 *
 * @author Eliška
 */

public class ByteUt {
    private ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);    

    public byte[] longToBytes(long x) {
        buffer.putLong(0, x);
        return buffer.array();
    }

    public long bytesToLong(byte[] bytes) {
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
    
    public byte[] join(byte[] b1, byte[] b2)
{
    byte[] all = new byte[b1.length + b2.length];

    System.arraycopy(b1, 0, all, 0, b1.length);
    System.arraycopy(b2, 0, all, b1.length, b2.length);

    return all;
}
}
